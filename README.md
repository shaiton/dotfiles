
#### "git aliases 

`git l`alias is the most used :

 * line numbering to ease `git rebase -i HEAD~N`
 * nice alignement
 * display only most important info
 * display only last 30 commits (exit directly)

`git lol` permits to display more history

![screenshot](gitalias.png)

